# Gemini

Plan of the program by Yuheng Feng:
<p align="center">  
<img src="https://gitlab.com/alopesmendes/unitexgemini/-/raw/master/Images/main.png" width=600px >  

## What is Gemini
Gemini is an annotation comparison module (in the XML or BRAT format) for Unitex/GramLab. This module can be used to
compare annotations between two texts. One is an annotated reference text TR semi-automatically and the other is a TH text automatically annotated by Unitex.

## Requirements
- Unitex: https://unitexgramlab.org/
- Ant: https://ant.apache.org/
- Maven: https://maven.apache.org/

## How to use Gemini
### In the terminal
To run Gemini via the terminal, first you will have to enter the
gemini-master folder. This folder contains a pom.xml file. This file
generates the gemini-X.X.X-SNAPSHOT-jar-with-dependencies.jar with
the command `mvn clean compile assembly:single`.
To facilitate the launch of the program, we rename the jar obtained in
gemini.jar . To compare the two texts in the terminal, you will have to
follow the following format:

`java -jar gemini.jar [-bratfileR 4 |-xmlfileR 5 ] [-bratfileH 2 |-xmlfileH 3 ] fileR (for more details about options, run java -jar gemini.jar --help or the README.md file in the gemini-master folder).`

### In GramLab IDE
To run Gemini via Gramlab IDE, enter the gemini-ide folder.
Type `ant` to generate GramLab.jar.
From now on you will be able to start GramLab IDE by entering the command `java -jar dist/GramLab.jar`.
Choose the project oriented perspective and click on the Gemini tab, then on Compare texts.
You can also find help on how to use the plugin by clicking on Help.
<p align="center">  
<img src="https://gitlab.com/alopesmendes/unitexgemini/-/raw/master/Images/1.png" >  
</p> 
Then, select the two texts you mant to compare.
<p align="center">  
<img src="https://gitlab.com/alopesmendes/unitexgemini/-/raw/master/Images/2_H.png" >  
</p> 
It is possible to type additional options.
<p align="center">  
<img src="https://gitlab.com/alopesmendes/unitexgemini/-/raw/master/Images/3.png" >  
</p> 
The results will be displayed in a new window.
<p align="center">  
<img src="https://gitlab.com/alopesmendes/unitexgemini/-/raw/master/Images/4.png" >  
</p> 

More information about GramLab itself can be found in the README.md file in gemini-ide folder.


## Authors
- Coline Mignot
- Philippe Gambette
- Yuheng Feng
- Ailton Lopes Mendes
- Kaïs Kingongo