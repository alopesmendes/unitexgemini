package org.gramlab.core.plugins.gemini;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

public class JCheckBoxOptions extends JFrame implements ActionListener {


    private JCheckBox[] scoreTypes;
    private StringBuilder builder;
    private JCheckBoxOptions(int width, int height, JCheckBox[] scoreTypes) {
        super("Additional options");
        this.scoreTypes = scoreTypes;
        builder = new StringBuilder();
        setSize( width, height);
        setLayout(null);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        for (int i = 0; i < scoreTypes.length; ++i) {
            if (scoreTypes[i].isSelected()) {
                builder.append(scoreTypes[i].getText()).append(" ");
            }
        }
        setVisible(false);
        dispose();
    }

    public static JCheckBoxOptions createBoxOptions() {
        JCheckBox[] scoreType = new JCheckBox[] {
                new JCheckBox("weakprecision"), new JCheckBox("strictprecision"), new JCheckBox("weightedprecision"),
                new JCheckBox("weakrecall"), new JCheckBox("strictrecall"), new JCheckBox("weightedrecall"),
                new JCheckBox("weakF-measure"), new JCheckBox("strictF-measure"), new JCheckBox("weightedF-measure")
        };
        JCheckBoxOptions boxOptions = new JCheckBoxOptions(800, 600, scoreType);
        JButton b;

        int y = 50, w = 200, h = 20;
        for (int i = 0; i < boxOptions.scoreTypes.length; ++i) {

            boxOptions.scoreTypes[i].setBounds(((i % 3) * 200) + 120, y, w, h);
            if (((i + 1) % 3) == 0) {
                y += 50;
            }

            boxOptions.add(boxOptions.scoreTypes[i]);
        }

        b=new JButton("Ok");
        b.setBounds(800 / 2 - 40,y + 50,80,30);
        b.addActionListener(boxOptions);


        boxOptions.add(b);
        return boxOptions;
    }

    @Override
    public String toString() {
        return builder.toString();
    }
}
