package org.gramlab.core.gramlab.project.config.locate;

public enum OutputsPolicy {
	IGNORE,
	MERGE,
	REPLACE
}
