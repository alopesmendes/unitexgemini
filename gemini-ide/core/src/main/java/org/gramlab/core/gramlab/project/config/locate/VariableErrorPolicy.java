package org.gramlab.core.gramlab.project.config.locate;

public enum VariableErrorPolicy {
	IGNORE,
	EXIT,
	BACKTRACK
}
