package org.gramlab.core.gramlab.project.config.concordance;

public enum ResultDisplay {
	INTERNAL_FRAME,
	HTML_VIEWER,
	TEXT_EDITOR;
}
