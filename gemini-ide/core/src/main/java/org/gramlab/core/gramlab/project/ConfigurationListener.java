package org.gramlab.core.gramlab.project;


public interface ConfigurationListener {
	
	public void configurationChanged();

}
