package org.gramlab.core.plugins.gemini;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;


public class Gemini {



    private static String falcutifOptions(JMenu m) {


        return JOptionPane.showInputDialog("Additional options : ");
    }

    private static String pathName(JMenu m, String file) {
        final JFileChooser fc = new JFileChooser();

        fc.setAcceptAllFileFilterUsed(false);
        fc.addChoosableFileFilter(new FileNameExtensionFilter(file+" format XML OR BRAT (*.xml|*.ann)", "xml", "ann"));
        fc.addChoosableFileFilter(new FileNameExtensionFilter(file+" format XML only (*.xml)", "xml"));
        fc.addChoosableFileFilter(new FileNameExtensionFilter(file+" format BRAT only (*.ann)", "ann"));
        fc.showOpenDialog(m);

        String fileToPath = "";
        File selectedFile = fc.getSelectedFile();
        if (selectedFile == null)
        {
            JOptionPane.showMessageDialog(null, "PLEASE SELECT 1 FILE!", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            fileToPath = selectedFile.getAbsolutePath();
        }
        return fileToPath;
    }

    private static String outputCommand(String command) {

        StringBuilder output = new StringBuilder();

        Process p;
        try {
            p = Runtime.getRuntime().exec(command);
            p.waitFor();
            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line;
            while ((line = reader.readLine())!= null) {
                output.append(line).append("\n");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return output.toString();

    }

    private static String findJarGemini() {
        Path p = Paths.get("/home");
        final int maxDepth = 50;
        try (Stream<Path> matches = Files.find(p, maxDepth, (path, basicFileAttributes) -> path.toString().endsWith("dist/lib/gemini.jar"))) {

            return matches.map(path -> path.toString()).findFirst().orElse("");
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        return "";
    }

    private static String commandName(JMenu m) throws InterruptedException {

        String jar = findJarGemini();
        if (jar.isEmpty()) {
            return "";
        }

        String fileH = pathName(m, "fileH");
        if (fileH.isEmpty()) {
            return "";
        }
        String optionH = (fileH.endsWith(".ann") ? "-bratfileH" : "-xmlfileH") + " " + fileH;

        String fileR = pathName(m, "fileR");
        if (fileR.isEmpty()) {
            return "";
        }
        String optionR = (fileH.endsWith(".ann") ? "-bratfileR" : "-xmlfileR") + " " + fileR;

        String options = falcutifOptions(m);
        return "java -jar " + jar + " " + optionH + " " + optionR + " " + options;
    }

    private static void execCommand(String command) {
        if (command.isEmpty()) {
            return;
        }

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(outputStream);
        PrintStream old = System.out;
        System.setOut(ps);
        System.setErr(ps);

        System.out.println(outputCommand(command));

        System.out.flush();
        System.setOut(old);

        JOptionPane.showMessageDialog(null, outputStream.toString(), "Information", JOptionPane.INFORMATION_MESSAGE);
    }

    public JMenu AddMenu() {

        JMenu m = new JMenu("Gemini");

        JMenuItem tr = new JMenuItem(new AbstractAction("Compare texts"){
            /*
             * Get parameters from Gramlab
             */
            public void actionPerformed(ActionEvent e) {


                String command = "";
                try {
                    command = commandName(m);
                } catch (InterruptedException exception) {
                    exception.printStackTrace();
                }

                execCommand(command);
                //falcutifOptions(this);
            }
        });

        JMenuItem help = new JMenuItem(new AbstractAction("Help") {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String command = "java -jar " + findJarGemini() + " --help";
                execCommand(command);
            }
        });

        m.add(tr);
        m.add(help);
        return m;
    }

}


