package org.gramlab.core.gramlab.project.config.locate;

public enum MatchesPolicy {
	SHORTEST,
	LONGEST,
	ALL
}
