/**
 * <p>This package will contain different classes which roles will be to apply different algorithms.</p>
 *	
 * For starters the package has the {@link algo.LevenshteinDistance} class which will apply the Levenshtein distance algorithm.
 * <br>
 * For now this package does not have any test since there's only one class and it was tested directly with a main.
*/
package algo;