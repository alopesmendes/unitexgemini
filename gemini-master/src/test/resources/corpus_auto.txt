
/********************************************************************************/
/****                  Ces données sont fournies gracieusement par           ****/
/****                  le Groupe d'Action Locale Othe-Armance                ****/
/***                   (http://www.tourisme-othe-armance.com/)               ****/
/***                qui en possède le Copyright, en partenariat avec         ****/
/***               les offices du tourisme présents sur ce territoire.       ****/
/* Elles peuvent être diffusées sous réserve de conserver la présente notice.   */
/***                Ces données sont distribuées sous licence  LGPLLR        ****/ 
/*(http://infolingu.univ-mlv.fr/DonneesLinguistiques/Lexiques-Grammaires/lgpllr.html)*/
/********************************************************************************/
 
Juin 2011 : ESSOYES – Exposition permanente de M. Monnet Pierre, peintre du Pays d’Armance « Au fil de l’Ours, dans les pas de Renoir » à l’atelier du 2 rue du four.
 
21 juin : RACINES – Fête de la musique organisée par l’Amicale Racinoise 03 25 70 67 51.
SAINT MARDS-EN-OTHE - Concert des Choristes en Othe sous la direction de Nicolaï Cok en l'église à 20 h.
 
23 juin : AIX-EN-OTHE - Concert orgue et soprano  : le duo Michaël Matthès et Claire Farge à 20 h 30 en l'église. Entrée libre. 
 
23 juin au 7 juillet : Cabaret itinérant du Festival en Othe. Programme sur le www.festivalenothe.org
 
Du 23 au 27 juin : COUSSEGREY – Stage de dessin Mandalas. Relier les yeux, les mains et l’inspiration ! Pour cela, deux mouvements alternent et se complètent l'un l'autre. Tarif 355 €. Renseignements au 03 25 70 69 35 ou www.laurier-rouge.com
 
Du 24 au 26 juin : ERVY-LE-CHATEL – Stage d’Aquarelle à l’Atelier de Sylvie Vernageau. Stage intensif de 9 h 30 à 12 h 30 et de 14 h 30 à 18 h. Tarif : 150 €. Réservations et informations au 03 25 42 10 46 ou 06 82 96 92 24.
 
24 juin : SAINT-MARDS-EN-OTHE –Feux de la Saint Jean à l’entrée de Saint Mards direction Aix-en-Othe. Organisé par la MTP Le Lavoir.
AIX-EN-OTHE - Concert des Choristes en Othe sous la direction de Nicolaï Cok en l'église à 20 h.
 
25 juin : AUXON – Fête scolaire – Kermesse aux écoles maternelle et primaire. 03 25 42 03 43.
MARAYE-EN-OTHE – Feux de la Saint-Jean au lavoir des boulins à 19 h. Animations musicales, restauration sur place. Organisé par Maraye Pour Tous 03 25 80 75 69
NEUVILLE –SUR-VANNE – Feux de la Saint Jean et soirée champêtre du Comité des fêtes.
RACINES – Feux de la Saint-Jean. Organisé par le comité des fêtes.
BERULLE - Randonnée de 9 h 30 à 15 h (prévoir pique-nique) « A la rencontre d’arbres méconnus » Départ et retour à Bérulle - Participation 5 € et 3 € (adhérents). Organisé par l'association Libre Voix 03 25 42 56 34.
BERULLE - Projection à 15 h 30 (durée 52’ suivie d’un temps d’échange). Film Documentaire « Bio-attitude sans béatitude » de Olivier Sarrazin, produit par Réal Productions. Entrée 5 € et 3 € (adhérents). Organisé par l'association Libre Voix 03 25 42 56 34.
BUCEY-EN-OTHE - Concours de pétanque amateur  à partir de 14 h 30 au stade. 
 
26 juin : CHAMOY – Vide greniers sur la place de l’église de 7 h à 19 h. Organisé par Chamoy Animations 03 25 81 58 09.
BUCEY-EN-OTHE - Vide grenier sur le stade de l'Orée d'Othe. Organisé par le FC Bucey, contact 03 25 70 32 17 ou 06 77 07 38 97.
VOSNON – Concours de cidre. Dégustation et vote du public dans le centre de Vosnon. Organisé par Vosnon Loisirs 03 25 41 52 68 et l’Amicale des Sapeurs Pompiers 03 25 42 02 25.
PALIS – Rally / Jeux de piste au départ de la place Gambetta. Promenez-vous à pied dans Pâlis à la recherche d’énigmes. Organisé par Pâlis Animation Loisirs 03 25 40 21 61.
CHAOURCE – Concert d’orgue à l’église par Alain Agazzi de Saint-Florentin. Concert à 17h.
VILLEMOIRON-EN-OTHE - Vide-grenier sur la place de l'Église à partir de 8 h 30. 2 €/mètre. Organisé par le comité des fêtes 03 25 80 32 62


